OVA Module:
-----------

Open Video Annotation Module allows rendering Video and Audio separate nodes
with Field Formatters like OVA Video Formatter and OVA Audio Formatter.

This module support Annotations by using VIDEOJS Plugin over video and audio.
We can store the annotations by using Annotation Module in drupal 8
https://www.drupal.org/project/annotation_store.

This module supports Textfield formatter which plays the video and audio as a
link which will given in the textfields.

Installation Instructions:
-------------------------
1. Install this modules in modules/contrib folder.
2. Create a 'text' fields for Audio and Video.
3. Choose the formatters like Ova Audio Formatter & Ova Video Formatter.
4. Allowable formats for Video: mp4, ogg, youtube, webm.
5. Allowable formats for Audio: mp3

Future Enhancements:
--------------------
More extension support will available in future.